package in.donationclub.smartstorage;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link frag4.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link frag4#newInstance} factory method to
 * create an instance of this fragment.
 */
public class frag4 extends Fragment implements RecyclerItemClickListener.OnRecyclerClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private RecyclerItemClickListener recyclerItemClickListener;
    private ArrayList<FaqInfo> itemList;
    private SessionManager sessionManager;
    private RestController restController;
    private FaqAdapter recyclerViewAdapter;
    private static final String TAG = "frag4";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public frag4() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment frag4.
     */
    // TODO: Rename and change types and number of parameters
    public static frag4 newInstance(String param1, String param2) {
        frag4 fragment = new frag4();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.faq_frag, container, false);
        init(v);
        restController.getFaq(sessionManager,frag4.this, getContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, this));
        recyclerViewAdapter = new FaqAdapter(getContext(), itemList);
        recyclerView.setAdapter(recyclerViewAdapter);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
public void init(View v){
        recyclerView = v.findViewById(R.id.faq_recycler);
        itemList = new ArrayList<>();
        sessionManager = new SessionManager(getActivity());
        restController = new RestController();
}

void onDownloadComplete(ArrayList<FaqInfo> op){
    Log.e(TAG, "onDownloadComplete: " + op.toString() );
        itemList.clear();
        itemList.addAll(op);
        recyclerViewAdapter.notifyDataSetChanged();
}
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.e(TAG, "onItemClick: Touch working" );
        FaqInfo info = recyclerViewAdapter.getItem(position);
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.faq_answer);
        dialog.setTitle("Title...");
        int width = (int) (getResources().getDisplayMetrics().widthPixels);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.60);
        dialog.getWindow().setLayout(width, height);
        TextView answer = dialog.findViewById(R.id.answer);
        answer.setText(info.getAnswer());
        Button onBtn = dialog.findViewById(R.id.button_faq_answer);
        onBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

//        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.80);
//        int height = (int) (getResources().getDisplayMetrics().heightPixels );
//        dialog.getWindow().setLayout(width, height);



          dialog.show();


    }


    @Override
    public void onItemLongClick(View view, int position) {

    }
}
