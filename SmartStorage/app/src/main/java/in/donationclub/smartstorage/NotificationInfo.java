package in.donationclub.smartstorage;

public class NotificationInfo {

    private String notificationText;
    private String notificationDate;
    private String notificationTime;
    private String itemName;
    private int severity;

    public NotificationInfo(String notificationText, String notificationDate, String notificationTime, String itemName, int severity) {

        this.notificationText = notificationText;
        this.notificationDate = notificationDate;
        this.notificationTime = notificationTime;
        this.itemName = itemName;
        this.severity = severity;
    }



    public String getNotificationText() {
        return notificationText;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public String getItemName() {
        return itemName;
    }

    public int getSeverity() {
        return severity;
    }
}
