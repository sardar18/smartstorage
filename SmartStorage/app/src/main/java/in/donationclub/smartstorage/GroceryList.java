package in.donationclub.smartstorage;

import android.content.DialogInterface;
import android.content.Intent;
import android.se.omapi.Session;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.View;

import java.io.Serializable;
import java.util.ArrayList;

public class GroceryList extends AppCompatActivity implements RecyclerItemClickListener.OnRecyclerClickListener{

    private RecyclerView recyclerView;
    private SessionManager sessionManager;
    private ArrayList<GrocerylistInfo> itemList;
    private GroceryListAdapter recyclerViewAdapter;
    private RestController restController;
    private static final String TAG = "GroceryList";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_list);
        init();
        restController.getGroceryLists(this, this, sessionManager);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this));
        recyclerViewAdapter = new GroceryListAdapter(this, itemList);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    public void init(){
        recyclerView = findViewById(R.id.grocery_list_recycler);
        sessionManager = new SessionManager(this);
        itemList = new ArrayList<>();
        restController = new RestController();
    }
    public void onDownloadComplete(ArrayList<GrocerylistInfo> arr){
        itemList.clear();
        itemList.addAll(arr);
        itemList.add(new GrocerylistInfo("6969","Generate New",new ArrayList<Pair<String, Double>>(),"akaka"));
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View view, int position) {

        GrocerylistInfo info = recyclerViewAdapter.getInfo(position);
        ArrayList<Pair<String,Double>> items = info.getItems();
        String itemName = info.getGroceryname();
        String id = info.getGroceryListId();
        Log.e(TAG, "onItemClick: size check " + items.size() );
        ArrayList<GroceryItemsInfo> itemList = new ArrayList<>();
        for(int i = 0;i<items.size();i++)
            itemList.add(new GroceryItemsInfo(items.get(i)));

        Intent intent = new Intent(GroceryList.this, Groceries.class);
        intent.putExtra("items",itemList);
        intent.putExtra("name",itemName);
        intent.putExtra("id",id);
        startActivity(intent);


    }


    @Override
    public void onItemLongClick(View view, int position) {

    }
}
