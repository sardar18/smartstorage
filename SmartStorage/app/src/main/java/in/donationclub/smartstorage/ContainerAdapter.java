package in.donationclub.smartstorage;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Arrays;

public class ContainerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final String TAG = "ContainerAdapter";
    private ArrayList<ContainerInfo> itemList = null;
    private Context mContext;
   

    public ContainerAdapter(ArrayList<ContainerInfo> itemList, Context mContext) {
        this.itemList = itemList;
        this.mContext = mContext;
        

    }

   
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_record, parent, false);
            RecyclerViewHolder holder =  new RecyclerViewHolder(v);
            holder.setIsRecyclable(false);
            return holder;
        

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
       
                RecyclerViewHolder holder = (RecyclerViewHolder) viewHolder;
                ContainerInfo photoItem = itemList.get(position);
                holder.cid.setText(photoItem.getContainerId());
                holder.weight.setText(Double.toString(photoItem.getCurrentWeight()));
                holder.threshold.setText(Double.toString(photoItem.getThreshold()));
                holder.rem.setText(Double.toString(photoItem.getRemaining()));
                holder.itemName.setText(photoItem.getItemName());
                ArrayList<Entry> yvalues = new ArrayList<Entry>();
                yvalues.add(new Entry((float)photoItem.getRemaining(), 0));
        yvalues.add(new Entry((float)photoItem.getUsedWeight(), 1));

        PieDataSet dataSet = new PieDataSet(yvalues, "Usage");
        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add("Rem");
        xVals.add("Used");
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        holder.pieChart.setDescription("");
        holder.pieChart.setDrawHoleEnabled(false);
        holder.pieChart.setData(data);



    }

   
    @Override
    public int getItemCount() {
//        Log.e(TAG, "getItemCount: " + itemListFiltered.get() );
        return itemList.size();
    }
    
    
   
    static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView cid;
        TextView rem;
        TextView threshold;
        TextView weight;
        TextView itemName;
        PieChart pieChart;
       

        public RecyclerViewHolder(View v){
            super(v);
            cid = v.findViewById(R.id.cid);
            rem = v.findViewById(R.id.rem);
            weight = v.findViewById(R.id.weight);
            threshold = v.findViewById(R.id.threshold);
            itemName = v.findViewById(R.id.itemName);
            pieChart = v.findViewById(R.id.piechart);
            pieChart.setUsePercentValues(true);
            
        }


       
    }
   
}
