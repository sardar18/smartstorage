package in.donationclub.smartstorage;

public class Connection {
    private static String server = "http://137.116.144.69:3000/";
    private static String notifications = "getNotifications";
    private static String groceryList = "allGroceryLists";
    private static String getFaq = "getFaq";
    private static String usage = "getUsage";
    private static String login = "login";
    private static String signUp = "signUp";
    private static String verifyOtp = "verifyOtp";
    private static String updateDetails = "updateUserDetails";
    private static String logOut = "logOut";

    public static String getLogOut() {
        return server+logOut;
    }

    public static String getUpdateDetails() {
        return server+updateDetails;
    }

    public static String getVerifyOtp() {
        return server+verifyOtp;
    }

    public static String getSignUp() {
        return server+signUp;
    }

    public static String getLogin() {
        return server+login;
    }

    public static String getNotifications() {
        return server+notifications;
    }
    public static String getAllGroceryLists() {return server+groceryList;}
    public static String getFaq() { return server+getFaq ;}

    public static String getUsage() {
        return server+usage;
    }
}
