package in.donationclub.smartstorage;

public class FaqInfo {
    private String questionId;
    private String question;
    private String answer;

    public String getQuestionId() {
        return questionId;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public FaqInfo(String questionId, String question, String answer) {
        this.questionId = questionId;
        this.question = question;
        this.answer = answer;
    }
}
