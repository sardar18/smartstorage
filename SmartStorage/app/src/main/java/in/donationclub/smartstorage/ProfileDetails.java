package in.donationclub.smartstorage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ProfileDetails extends AppCompatActivity {
    private EditText firstName;
    private EditText lastName;
    private EditText gender;
    private EditText dob;
    private Button btnUpdate;
    private SessionManager sessionManager;
    private RestController restController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);
        init();
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateDetails();
            }
        });
    }

    private void updateDetails() {
        if(firstName.getText().toString().matches(""))
            Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
        else if(lastName.getText().toString().matches(""))
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
        else if(gender.getText().toString().matches(""))
            Toast.makeText(this, "Enter Gender", Toast.LENGTH_SHORT).show();
        else if(firstName.getText().toString().matches(""))
            Toast.makeText(this, "Enter Date of Birth", Toast.LENGTH_SHORT).show();
        else
            restController.updateUserDetails(ProfileDetails.this, sessionManager, this, firstName.getText().toString(),
                    lastName.getText().toString(), gender.getText().toString(), dob.getText().toString());

    }


    private void init() {
        firstName = findViewById(R.id.firstname);
        lastName = findViewById(R.id.lastname);
        gender = findViewById(R.id.gender);
        dob = findViewById(R.id.dob);
        btnUpdate = findViewById(R.id.btnUpdate);
        sessionManager = new SessionManager(this);
        restController = new RestController();
    }
    public void onSuccess(){
        Intent i = new Intent(ProfileDetails.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
