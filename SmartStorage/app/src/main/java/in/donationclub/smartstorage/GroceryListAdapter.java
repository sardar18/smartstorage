package in.donationclub.smartstorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class GroceryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<GrocerylistInfo> items;

    public GroceryListAdapter(Context mContext, ArrayList<GrocerylistInfo> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.faq_question_layout, viewGroup, false);
        ListHolder holder =  new ListHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ListHolder holder = (ListHolder) viewHolder;
        GrocerylistInfo info = items.get(i);
        holder.itemName.setText(info.getGroceryname());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public GrocerylistInfo getInfo(int pos) { return items.get(pos); }

    static class ListHolder extends RecyclerView.ViewHolder{
     TextView itemName;

     public ListHolder(View v) {
         super(v);
         this.itemName = v.findViewById(R.id.question);
     }
 }


}
