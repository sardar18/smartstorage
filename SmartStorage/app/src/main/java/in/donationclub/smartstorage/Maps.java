package in.donationclub.smartstorage;

import java.util.HashMap;

public class Maps {

    private HashMap<Integer,Integer> severityMap =  new HashMap<>();


    public Maps() {
        severityMap.put(1,R.drawable.notification_yellow);
        severityMap.put(2,R.drawable.notification_red);
        severityMap.put(3,R.drawable.notification_green);
    }

    public int getSeverity(int n){
        return severityMap.get(n);
    }
}
