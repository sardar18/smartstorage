package in.donationclub.smartstorage;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Frag1.OnFragmentInteractionListener,
Frag2.OnFragmentInteractionListener, NotificationFrag.OnFragmentInteractionListener, frag4.OnFragmentInteractionListener{

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_home_white_24dp,
            R.drawable.ic_add_box_black_24dp,
            R.drawable.ic_notifications_black_24dp,
            R.drawable.ic_question_answer_black_24dp
    };
    private FloatingActionButton botBtn;
    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    private ArrayList<BaseMessage> messageList;
    private ImageView proImg;
    private static final String TAG = "MainActivity";
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botBtn = findViewById(R.id.botBtn);
sessionManager = new SessionManager(this);
        if(!sessionManager.getLogin()){
            Intent i = new Intent(MainActivity.this, LoginAcctivity.class);
            startActivity(i);
            finish();
        }
        else if(sessionManager.getLogin() && !sessionManager.getProfile()){
            Intent i = new Intent(MainActivity.this, ProfileDetails.class);
            startActivity(i);
            finish();
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//                toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//                setLang(session.getLang());

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        proImg = header.findViewById(R.id.navHeaderImage);
        Picasso.get().load("https:////farm5.staticflickr.com/4916/44243886200_9aec39cf5c_m.jpg")
                .error(R.drawable.ic_image_black_24dp)
                .placeholder(R.drawable.ic_image_black_24dp)
                .into(proImg);
        proImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent i = new Intent(MainActivity.this, CompletePhotoActivity.class);
                    i.putExtra("image", "https:////farm5.staticflickr.com/4916/44243886200_9aec39cf5c_m.jpg");
                    startActivity(i);

            }
        });
        TextView proName = header.findViewById(R.id.navHeaderName);
        proName.setText(sessionManager.getUserName());
        FirebaseApp.initializeApp(this);
        Log.e(TAG, "onCreate: Token is " + FirebaseInstanceId.getInstance().getToken() );
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new Frag1(), "Home", tabIcons[0]);
        adapter.addFragment(new Frag2(), "Add New", tabIcons[1]);
        adapter.addFragment(new NotificationFrag(), "Notifications", tabIcons[2]);
        adapter.addFragment(new frag4(), "FAQ", tabIcons[3]);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        highLightCurrentTab(0); // for initial selected tab view
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                highLightCurrentTab(position); // for tab change
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        botBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.chat_popup);
                dialog.setTitle("BotBot");
                messageList = new ArrayList<BaseMessage>();
                messageList.add(new BaseMessage("Hey.!\nMy name is BotBot and you can ask me any questions about the app or its usage."
                        , "2", "6:00"));
                messageList.add(new BaseMessage("Tell me the name of this app.?","1","6:01"));
                messageList.add(new BaseMessage("Its not decided yet these developers are still arguing over it.", "2", "6:01"));
//                Log.e(TAG, "onCreate: size is " + messageList.size() );
                mMessageRecycler = (RecyclerView) dialog.findViewById(R.id.reyclerview_message_list);
                mMessageAdapter = new MessageListAdapter(MainActivity.this, messageList);
                mMessageRecycler.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                mMessageRecycler.setAdapter(mMessageAdapter);
                int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.95);
                int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.80 );
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(width, height);
                dialog.show();
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Intent i;
        switch(menuItem.getItemId()) {
            case R.id.nav_profile:
                i = new Intent(MainActivity.this, Profile.class);
                startActivity(i);
                return true;
            case R.id.grocery:
                i = new Intent(MainActivity.this, GroceryList.class);
                startActivity(i);
                return true;
            case R.id.usage:
                i = new Intent(MainActivity.this, VisualizationsActivity.class);
                startActivity(i);
                return true;
            case R.id.settings:
                i = new Intent(MainActivity.this, Settings.class);
                startActivity(i);
                return true;
        }
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
//        MenuItem item=menu.findItem(R.id.logOut); // here itemIndex is int
//        item.setTitle("YourTitle");
//        this.menu = menu;
//        updateMenuTitles();

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.logOut) {
            logOut();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logOut() {
        RestController restController = new RestController();
        restController.logOut(sessionManager, MainActivity.this, this);
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i, this));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position, this));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent i = new Intent(MainActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void onSuccess(){
        Toast.makeText(this, "LogOut Successful", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, LoginAcctivity.class);
        startActivity(i);
        finish();
    }
}

