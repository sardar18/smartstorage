package in.donationclub.smartstorage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SignUp extends AppCompatActivity {
    private TextView transit;
    private TextView phone;
    private TextView conPassword;
    private TextView password;
    private Button signUpBtn;
    private SessionManager sessionManager;
    private RestController restController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sessionManager = new SessionManager(this);
        restController = new RestController();
        transit = findViewById(R.id.tvSignIn);
        transit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignUp.this, LoginAcctivity.class);
                startActivity(i);
            }
        });
        phone = findViewById(R.id.atvUsernameReg);
        conPassword = findViewById(R.id.atvPasswordReg);
        password = findViewById(R.id.atvEmailReg);
        signUpBtn = findViewById(R.id.btnSignUp);
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOtp();
            }
        });
    }

    public void sendOtp(){
    if(phone.getText().toString().matches(""))
        Toast.makeText(this, "Enter Phone",Toast.LENGTH_SHORT).show();
    else if(conPassword.getText().toString().matches(""))
        Toast.makeText(this, "Enter Password",Toast.LENGTH_SHORT).show();
    else if(password.getText().toString().matches(""))
        Toast.makeText(this, "Enter Password",Toast.LENGTH_SHORT).show();
    else if(!password.getText().toString().matches(conPassword.getText().toString()))
        Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show();
    else {
        restController.signUp(sessionManager, this, phone.getText().toString(), password.getText().toString(), SignUp.this);

    }

    }
    public void changeActivity(){

            sessionManager.setUser(phone.getText().toString());

            Intent i = new Intent(SignUp.this, OTPActivity.class);
            i.putExtra("phone", phone.getText().toString());
            i.putExtra("password", password.getText().toString());
            startActivity(i);
            finish();

    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
