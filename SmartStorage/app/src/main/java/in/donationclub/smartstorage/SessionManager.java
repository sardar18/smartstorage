package in.donationclub.smartstorage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionManager {

    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "AppLogin";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public void setUser(String user){
        editor.putString("user",user);
        editor.commit();
    }
    public String getUser(){
        return pref.getString("user","");

    }
    public void setToken(String token){
        editor.putString("token",token);
        editor.commit();
    }
    public String getToken(){
        return pref.getString("token","");
    }
    public void setUserName(String userName){
        editor.putString("name",userName);
        editor.commit();
    }
    public String getUserName(){
        return pref.getString("name","");
    }

    public void setRes(boolean user){
        editor.putBoolean("res",user);
        Log.e("Session", "setRes: Setting " + user );
        editor.commit();
    }
    public boolean getRes(){
        return pref.getBoolean("res",false);

    }

    public void setLogin(boolean user){
        editor.putBoolean("loggedIn",user);
        editor.commit();
    }
    public boolean getLogin(){
        return pref.getBoolean("loggedIn",false);

    }

    public void setProfile(boolean user){
        editor.putBoolean("profile",user);
        editor.commit();
    }
    public boolean getProfile(){
        return pref.getBoolean("profile",false);

    }
}
