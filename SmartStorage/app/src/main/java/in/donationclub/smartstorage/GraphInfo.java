package in.donationclub.smartstorage;

import java.util.Date;

public class GraphInfo {
    String date;
    double weight;

    public GraphInfo() {
    }

    public GraphInfo(String date, double weight) {
        this.date = date;
        this.weight = weight;
    }

    public String getDate() {
        return date;
    }

    public double getWeight() {
        return weight;
    }
}
