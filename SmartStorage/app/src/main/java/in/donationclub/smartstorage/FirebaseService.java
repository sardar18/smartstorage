package in.donationclub.smartstorage;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;

public class FirebaseService extends FirebaseMessagingService {

    private static final String TAG = "FirebaseIDService";
    private SessionManager sessionManager;


    @Override
    public void onNewToken(String token) {
        Log.e(TAG, "Refreshed token: " + token);
        sessionManager = new SessionManager(this);
//        sessionManager.setFirebaseToken(token);


        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(token);
    }
}
