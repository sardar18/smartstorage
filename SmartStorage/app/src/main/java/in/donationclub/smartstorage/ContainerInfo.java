package in.donationclub.smartstorage;

public class ContainerInfo {
    private String containerId;
    private String itemName;
    private double currentWeight;
    private double threshold;
    private double remaining;
    private boolean isFallen;
    private double usedWeight;


    public ContainerInfo(String containerId, String itemName, double currentWeight, double threshold, double remaining, boolean isFallen, double usedWeight) {
        this.containerId = containerId;
        this.itemName = itemName;
        this.currentWeight = currentWeight;
        this.threshold = threshold;
        this.remaining = remaining;
        this.isFallen = isFallen;
        this.usedWeight = usedWeight;
    }

    public String getContainerId() {
        return containerId;
    }

    public String getItemName() {
        return itemName;
    }

    public double getCurrentWeight() {
        return currentWeight;
    }

    public double getThreshold() {
        return threshold;
    }

    public double getRemaining() {
        return remaining;
    }

    public boolean isFallen() {
        return isFallen;
    }

    public double getUsedWeight() {
        return usedWeight;
    }
}
