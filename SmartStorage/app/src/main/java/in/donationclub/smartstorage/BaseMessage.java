package in.donationclub.smartstorage;

public class BaseMessage {

    String msg;
    String uid;
    String time;

    public String getMsg() {
        return msg;
    }

    public String getUid() {
        return uid;
    }

    public String getTime() {
        return time;
    }

    public BaseMessage(String msg, String uid, String time) {
        this.msg = msg;
        this.uid = uid;
        this.time = time;
    }
}
