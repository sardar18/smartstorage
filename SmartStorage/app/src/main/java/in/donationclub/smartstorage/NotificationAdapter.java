package in.donationclub.smartstorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.RecyclerViewHolder>{

    private Context mContext;
    private ArrayList<NotificationInfo> items;
    private static final String TAG = "NotificationAdapter";
    private Maps maps;

    public NotificationAdapter(Context mContext, ArrayList<NotificationInfo> items) {
        this.mContext = mContext;
        this.items = items;
        Log.e(TAG, "NotificationAdapter: size" + items.size() );
        this.maps = new Maps();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_layout, viewGroup, false);
        return new NotificationAdapter.RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, int i) {
        NotificationInfo info = items.get(i);
        recyclerViewHolder.itemName.setText(info.getItemName());
        recyclerViewHolder.notificationDate.setText(info.getNotificationDate());
        recyclerViewHolder.notificationText.setText(info.getNotificationText());
        recyclerViewHolder.notificationImage.setImageResource(maps.getSeverity(info.getSeverity()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;
        TextView notificationText;
        TextView notificationDate;
        ImageView notificationImage;
        public RecyclerViewHolder(View v){
            super(v);
            itemName = v.findViewById(R.id.notification_item_name);
            notificationText = v.findViewById(R.id.notification_item_text);
            notificationDate = v.findViewById(R.id.notification_date);
            notificationImage = v.findViewById(R.id.notification_image);
        }
    }
}
