package in.donationclub.smartstorage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.widget.TextView;

import java.util.ArrayList;

public class Groceries extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView title;
    private SessionManager sessionManager;
    private RestController restController;
    private GroceriesAdapter recyclerViewAdapter;
    private static final String TAG = "Groceries";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groceries);
        init();
        Intent i = getIntent();
//        if(i.getIntExtra("flag",0) == 121){
        Intent intent = getIntent();
        ArrayList<Pair<String, Double>> items = (ArrayList<Pair<String, Double>>)i.getSerializableExtra("items");
        Log.e(TAG, "onCreate: size is " + items.size() );
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
//            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(Groceries.this, recyclerView, this));
//            recyclerViewAdapter = new GroceriesAdapter(this, info.getItems());
//            recyclerView.setAdapter(recyclerViewAdapter);
//        }

    }

    public void init(){
        recyclerView = findViewById(R.id.grocery_items_recycler);
        title = findViewById(R.id.grocery_list_name);
        sessionManager = new SessionManager(this);
        restController = new RestController();
    }
}
