package in.donationclub.smartstorage;

import android.os.Bundle;
import android.util.Pair;

import java.io.Serializable;
import java.util.ArrayList;

public class GrocerylistInfo {
    String userId;
    String groceryname;
    String groceryListId;
    ArrayList<Pair <String, Double>> items;

    public GrocerylistInfo(String userId, String groceryname, ArrayList<Pair<String, Double>> items, String groceryListId) {
        this.userId = userId;
        this.groceryname = groceryname;
        this.items = items;

        this.groceryListId = groceryListId;
    }

    public String getGroceryListId() {
        return groceryListId;
    }

    public ArrayList<Pair<String, Double>> getItems() {
        return items;
    }

    public String getUserId() {
        return userId;
    }

    public String getGroceryname() {
        return groceryname;
    }
}
