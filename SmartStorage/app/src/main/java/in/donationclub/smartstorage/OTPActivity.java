package in.donationclub.smartstorage;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class OTPActivity extends AppCompatActivity {
    private TextView timer;
    private Button btnResend;
    private Button btnVerify;
    private EditText otp;
    private static final String TAG = "OTPActivity";
    private String phone;
    private SessionManager session;
    private ProgressBar spinner;
    private String password;
    private String confirmpassword;
    private RestController restController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        session = new SessionManager(this);
        timer = findViewById(R.id.timer);
        btnResend = findViewById(R.id.btnResend);
        btnVerify = findViewById(R.id.verifyBtn);
        otp = findViewById(R.id.inputOtp);
        spinner = findViewById(R.id.progressBarOtp);
        spinner.setVisibility(View.GONE);
        Intent i = getIntent();
        phone = i.getStringExtra("phone");
        password = i.getStringExtra("password");
        restController = new RestController();

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyOTP();
            }
        });
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                restController.signUp(session, OTPActivity.this, phone, password);

            }


        });

        MyCount counter = new MyCount(30000, 1000);
        counter.start();


    }

    public class MyCount extends CountDownTimer {

        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            btnResend.setActivated(false);
            btnResend.setEnabled(false);
        }

        @Override
        public void onFinish() {
            btnResend.setActivated(true);
            btnResend.setEnabled(true);
            timer.setText("");

        }

        @Override
        public void onTick(long millisUntilFinished) {

            timer.setText("Resend in " + millisUntilFinished / 1000);

        }

    }

    private void verifyOTP() {
        if (otp.getText().toString().matches(""))
            Toast.makeText(OTPActivity.this, "Enter OTP", Toast.LENGTH_SHORT).show();
        else {
            spinner.setVisibility(View.VISIBLE);
            JSONObject obj = new JSONObject();
            try {
                obj.put("phone", phone);
                obj.put("otp", otp.getText().toString());
            } catch (JSONException e) {
                Log.e(TAG, "verifyOTP: Error in setting JSON");
            }
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Connection.getVerifyOtp(), obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e(TAG, "onResponse: " + response.toString());
                    try {



                        if(response.has("type") && response.getString("type").matches("success")){
                            Intent i = new Intent(OTPActivity.this,ProfileDetails.class);
                            i.putExtra("position",0);
                            spinner.setVisibility(View.GONE);


                            startActivity(i);
                            finish();
                        }
                        else {
                            Toast.makeText(OTPActivity.this, "Error Verifying OTP", Toast.LENGTH_SHORT).show();
//                            Toast.makeText(OTPActivity.this, "Error Verifying OTP", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(OTPActivity.this, SignUp.class);
                            startActivity(i);
                            finish();
                        }


                    }
                    catch(JSONException e){
                        Toast.makeText(OTPActivity.this, "Error in Logging in",Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    spinner.setVisibility(View.GONE);
                    Toast.makeText(OTPActivity.this,"Error in Logging in",Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "onErrorResponse: " + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };
            Volley.newRequestQueue(this).add(post);

        }





    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
