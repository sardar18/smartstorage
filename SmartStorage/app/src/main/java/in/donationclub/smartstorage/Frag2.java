package in.donationclub.smartstorage;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Frag2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Frag2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Frag2 extends Fragment implements AdapterView.OnItemSelectedListener
{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private EditText containerIdAdd;
    private EditText itemNameAdd;
    private EditText maxWeightAdd;
    private EditText timeLapseAdd;
    private EditText thresholdAdd;
    private Button buttonAdd;
    private Spinner categoryAdd;
    private ArrayList<String> categoryList;
    private static final String TAG = "Frag2";

    private int selectedCategory = 1;
    private SessionManager sessionManager;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Frag2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Frag2.
     */
    // TODO: Rename and change types and number of parameters
    public static Frag2 newInstance(String param1, String param2) {
        Frag2 fragment = new Frag2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_frag2, container, false);
        inti(v);
        categoryAdd.setOnItemSelectedListener(this);
        categoryList = new ArrayList<>();
        categoryList.add("Choose");
        categoryList.add("General Grocery");
        categoryList.add("Special Grocery");
        categoryList.add("LPG Cylinders");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categoryList);
        categoryAdd.setAdapter(adapter);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addContainer();
            }

        });
        containerIdAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                containerIdAdd.setBackgroundResource(R.drawable.underline);
            }
        });
        itemNameAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemNameAdd.setBackgroundResource(R.drawable.underline);
            }
        });
        maxWeightAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                maxWeightAdd.setBackgroundResource(R.drawable.underline);
            }
        });
        timeLapseAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeLapseAdd.setBackgroundResource(R.drawable.underline);
            }
        });
        thresholdAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thresholdAdd.setBackgroundResource(R.drawable.underline);
            }
        });
        return v;
    }

    private void inti(View v) {
        containerIdAdd = v.findViewById(R.id.containerIdAdd);
        itemNameAdd = v.findViewById(R.id.itemNameAdd);
        maxWeightAdd = v.findViewById(R.id.maxWeightAdd);
        timeLapseAdd = v.findViewById(R.id.timeLapseAdd);
        thresholdAdd = v.findViewById(R.id.thresholdAdd);
        buttonAdd = v.findViewById(R.id.buttonAdd);
        categoryAdd = v.findViewById(R.id.categoryAdd);
        sessionManager = new SessionManager(getContext());
    }

    private void addContainer() {
        JSONObject obj = new JSONObject();
        if(checked()) {
            try {
                obj.put("containerId", containerIdAdd.getText().toString());
                obj.put("userId", sessionManager.getUser());
                obj.put("itemName", itemNameAdd.getText().toString());
                obj.put("maxWeight", maxWeightAdd.getText().toString());
                obj.put("threshold", thresholdAdd.getText().toString());
                obj.put("timeLapse", timeLapseAdd.getText().toString());
                obj.put("category", selectedCategory);

                JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, "http://137.116.144.69:3000/addContainer", obj, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getContext(), "Respponse is " + response.toString(), Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "Error in request " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                Volley.newRequestQueue(getContext()).add(post);
            } catch (JSONException e) {
                Log.e(TAG, "addContainer: Error in creating JSON");
            }
        }
    }

    private boolean checked() {
        if(containerIdAdd.getText().toString().matches("")){
            containerIdAdd.setBackgroundResource(R.drawable.border);
            itemNameAdd.setBackgroundResource(R.drawable.underline);
            maxWeightAdd.setBackgroundResource(R.drawable.underline);
            timeLapseAdd.setBackgroundResource(R.drawable.underline);
            thresholdAdd.setBackgroundResource(R.drawable.underline);
            Toast.makeText(getContext(), "Enter Container Id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(itemNameAdd.getText().toString().matches("")){
            containerIdAdd.setBackgroundResource(R.drawable.underline);
            itemNameAdd.setBackgroundResource(R.drawable.border);
            maxWeightAdd.setBackgroundResource(R.drawable.underline);
            timeLapseAdd.setBackgroundResource(R.drawable.underline);
            thresholdAdd.setBackgroundResource(R.drawable.underline);
            Toast.makeText(getContext(), "Enter Item Name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(maxWeightAdd.getText().toString().matches("")){
            containerIdAdd.setBackgroundResource(R.drawable.underline);
            itemNameAdd.setBackgroundResource(R.drawable.underline);
            maxWeightAdd.setBackgroundResource(R.drawable.border);
            timeLapseAdd.setBackgroundResource(R.drawable.underline);
            thresholdAdd.setBackgroundResource(R.drawable.underline);
            Toast.makeText(getContext(), "Enter Max. Weight", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(timeLapseAdd.getText().toString().matches("")){
            containerIdAdd.setBackgroundResource(R.drawable.underline);
            itemNameAdd.setBackgroundResource(R.drawable.underline);
            maxWeightAdd.setBackgroundResource(R.drawable.underline);
            timeLapseAdd.setBackgroundResource(R.drawable.border);
            thresholdAdd.setBackgroundResource(R.drawable.underline);
            Toast.makeText(getContext(), "Enter Time Lapse", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(thresholdAdd.getText().toString().matches("")){
            containerIdAdd.setBackgroundResource(R.drawable.underline);
            itemNameAdd.setBackgroundResource(R.drawable.underline);
            maxWeightAdd.setBackgroundResource(R.drawable.underline);
            timeLapseAdd.setBackgroundResource(R.drawable.underline);
            thresholdAdd.setBackgroundResource(R.drawable.border);
            Toast.makeText(getContext(), "Enter Threshold weight to be notified", Toast.LENGTH_SHORT).show();
            return false;
        }
        else
            return true;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        selectedCategory = i;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
