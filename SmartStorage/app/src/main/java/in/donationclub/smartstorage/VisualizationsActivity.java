package in.donationclub.smartstorage;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

public class VisualizationsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    private GraphView graph;
    private SessionManager sessionManager;
    private RestController restController;
    private ArrayList<GraphInfo> info;
    private double topLine;
    LineChartView lineChartView;
    private Spinner usageSpinner;
    private ArrayList<String> usageSpinnerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizations);
        init();

        usageSpinner.setOnItemSelectedListener(this);
        usageSpinnerList.add("Choose");
        usageSpinnerList.add("Last 7 Days");
        usageSpinnerList.add("Last 15 Days");
        usageSpinnerList.add("Last 30 Days");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, usageSpinnerList);
        usageSpinner.setAdapter(adapter);


    }
    public void init(){

        sessionManager = new SessionManager(this);
        restController = new RestController();
        info = new ArrayList<>();
        usageSpinner = findViewById(R.id.usage_spinner);
        usageSpinnerList = new ArrayList<>();
    }

    public void onDownloadComplete(ArrayList<GraphInfo> op, double topLine){
        info.clear();
        info.addAll(op);
        this.topLine = topLine;
        initializeGraph();
    }

    private void initializeGraph() {
        lineChartView = findViewById(R.id.chart);

        List yAxisValues = new ArrayList();
        List axisValues = new ArrayList();


        Line line = new Line(yAxisValues).setColor(Color.parseColor("#9C27B0"));

        for (int i = 0; i < info.size(); i++) {
            axisValues.add(i, new AxisValue(i).setLabel(info.get(i).getDate()));
        }

        for (int i = 0; i < info.size(); i++) {
            yAxisValues.add(new PointValue(i, (int)info.get(i).getWeight()));
        }

        List lines = new ArrayList();
        lines.add(line);

        LineChartData data = new LineChartData();
        data.setLines(lines);

        Axis axis = new Axis();
        axis.setValues(axisValues);
        axis.setTextSize(10);
        axis.setTextColor(Color.parseColor("#03A9F4"));
        data.setAxisXBottom(axis);

        Axis yAxis = new Axis();
        yAxis.setName("Usage in Grams");
        yAxis.setTextColor(Color.parseColor("#03A9F4"));
        yAxis.setTextSize(16);
        data.setAxisYLeft(yAxis);

        lineChartView.setLineChartData(data);
        Viewport viewport = new Viewport(lineChartView.getMaximumViewport());
        viewport.top = (int)topLine + 5;
        lineChartView.setMaximumViewport(viewport);
        lineChartView.setCurrentViewport(viewport);




    }
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(i != 0)
        restController.getUsage(sessionManager, this, VisualizationsActivity.this);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {


    }

}
