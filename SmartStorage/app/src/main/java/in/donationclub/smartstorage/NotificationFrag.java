package in.donationclub.smartstorage;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NotificationFrag.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NotificationFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationFrag extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private Spinner spinner;
    private NotificationAdapter recyclerViewAdapter;
    private ArrayList<String> spinnerData;
    private ArrayList<NotificationInfo> itemList;
    private SessionManager sessionManager;
    private RestController restController;
    private static final String TAG = "NotificationFrag";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public NotificationFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationFrag newInstance(String param1, String param2) {
        NotificationFrag fragment = new NotificationFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.notification_frag, container, false);
        init(v);
        spinnerData = new ArrayList<>();
        spinnerData.add("Last 7 Days");
        spinnerData.add("Last 15 Days");
        spinnerData.add("Last 30 Days");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),R.layout.notification_dropdown, spinnerData);
        dataAdapter.setDropDownViewResource(R.layout.notification_dropdown_style);
        spinner.setAdapter(dataAdapter);
        itemList = new ArrayList<>();
        restController.getNotifications(sessionManager, getContext(), NotificationFrag.this);
        Log.e(TAG, "onCreateView: size " + itemList );
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerView, this));
        recyclerViewAdapter = new NotificationAdapter(getContext(), itemList);
        recyclerView.setAdapter(recyclerViewAdapter);
        return v;
    }

    public void init(View v){
        recyclerView = v.findViewById(R.id.notification_recycler);
        spinner = v.findViewById(R.id.notification_spinner);
        sessionManager = new SessionManager(getContext());
        restController = new RestController();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void onDownloadComplete(ArrayList<NotificationInfo> info){
        itemList.clear();
        itemList.addAll(info);
        Log.e(TAG, "onDownloadComplete: size" + itemList.size() );
        recyclerViewAdapter.notifyDataSetChanged();
    }
}
