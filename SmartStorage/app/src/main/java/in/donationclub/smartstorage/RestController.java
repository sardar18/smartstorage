package in.donationclub.smartstorage;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class RestController {
    private static final String TAG = "RestController";

    public void getNotifications(final SessionManager sessionManager, Context context, final NotificationFrag notificationFrag){
        JSONObject obj = new JSONObject();
        String url = Connection.getNotifications();

        try {
            obj.put("userId", sessionManager.getUser());
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        ArrayList<NotificationInfo> output = new ArrayList<>();
                        Log.e(TAG, "onResponse: " + response.toString() );
                        JSONArray data = response.getJSONArray("data");
                        for(int i = 0;i<data.length();i++){
                            JSONObject object = data.getJSONObject(i);
                            String notificationText = object.getString("notificationText");
                            String notificationDate = object.getString("notificationDate");
                            String notificationTime = object.getString("notificationTime");
                            int severity = object.getInt("severity");
                            String itemName = object.getJSONArray("post").getJSONObject(0).getString("itemName");
                            //Log.e(TAG, "onResponse: text " + notificationText + " " + itemName );
                            NotificationInfo info = new NotificationInfo(notificationText,notificationDate,notificationTime,itemName,severity);
                            output.add(info);
                        }
                        Log.e(TAG, "getNotifications: size" + output.size() );
                        notificationFrag.onDownloadComplete(output);

                    }catch(JSONException e){
                        Log.e(TAG, "onResponse: Error in JSON" );
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "onErrorResponse: Error in response" );
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("authorization", sessionManager.getToken());

                    return params;
                }

            };
            Volley.newRequestQueue(context).add(post);
        }catch(JSONException e){
            Log.e(TAG, "getNotifications: Error in JSON" );
        }


    }

    public void getGroceryLists(Context mContext, final GroceryList groceryList, final SessionManager sessionManager){
        JSONObject obj = new JSONObject();
        String url = Connection.getAllGroceryLists();

        try{
            obj.put("userId","123");
        }catch(JSONException e){
            Log.e(TAG, "getGroceryLists: Error creating json" );
        }
        JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    Log.e(TAG, "onResponse: Response is " + response.toString() );
                    ArrayList<GrocerylistInfo> output = new ArrayList<>();

                    JSONArray arr = response.getJSONArray("data");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject object = arr.getJSONObject(i);
                        String groceryListId = object.getString("id");
                        String userId = object.getString("userId");
                        String groceryName =  object.getString("groceryName");
                        JSONArray groceries = object.getJSONArray("groceryItems");
                        ArrayList<Pair<String,Double>> groceryOutput = new ArrayList<>();
                        for(int ii = 0;ii<groceries.length();ii++){
                            JSONObject groceryObject = groceries.getJSONObject(ii);

                            Pair<String,Double> tmp = new Pair(groceryObject.getString("itemName"),groceryObject.getDouble("quantity"));
                            groceryOutput.add(tmp);
                            Log.e(TAG, "onResponse: tmp is " + tmp.toString() );

                        }
                        GrocerylistInfo info = new GrocerylistInfo(userId,groceryName,groceryOutput, groceryListId);
                        output.add(info);
                    }
                    groceryList.onDownloadComplete(output);
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: Error in response JSON");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: Server Error" );
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("authorization", sessionManager.getToken());

                return params;
            }

        };
        Volley.newRequestQueue(mContext).add(post);
    }

    public void getFaq(final SessionManager sessionManager, final frag4 callback,  Context mContext) {
        String url = Connection.getFaq();
        final ArrayList<FaqInfo> output = new ArrayList<>();
        JsonObjectRequest get = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arr = response.getJSONArray("data");
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj = arr.getJSONObject(i);
                        String id = obj.getString("questionId");
                        String question = obj.getString("question");
                        String answer = obj.getString("answer");
                        FaqInfo info = new FaqInfo(id, question, answer);
                        output.add(info);

                    }
                    callback.onDownloadComplete(output);
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: Error in response JSON");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: Server Error" );
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("authorization", sessionManager.getToken());

                return params;
            }
        };

        Volley.newRequestQueue(mContext).add(get);

    }
    public void getUsage(final SessionManager sessionManager, Context mContext, final VisualizationsActivity callback){
        try{
            JSONObject obj = new JSONObject();
            obj.put("containerId", "123");
            String url = Connection.getUsage();
            final ArrayList<GraphInfo> op = new ArrayList<>();
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray arr = response.getJSONArray("data");
                        double wt = -100000;
                        Log.e(TAG, "onResponse: length " + arr.length() );
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject object = arr.getJSONObject(i);
                            String d = object.getString("date");
                            double weight = object.getDouble("weight");
                            if(i == 0)
                                weight = 0;
                            if (i > 0)
                                weight = arr.getJSONObject(i-1).getDouble("weight") - weight;
                            if(weight < 0)
                                weight = 0;

                            GraphInfo info = new GraphInfo(d, weight);
                            if(weight > wt)
                                wt = weight;
                            Log.e(TAG, "onResponse: Added " + Double.toString(weight) );
                            op.add(info);
                        }
                        callback.onDownloadComplete(op, wt);
                    } catch (JSONException e) {
                        Log.e(TAG, "onResponse: Error in JSON");
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "onErrorResponse: Error in JSON" );
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("authorization", sessionManager.getToken());

                    return params;
                }
            };
            Volley.newRequestQueue(mContext).add(post);
        }catch(JSONException e){
            Log.e(TAG, "getUsage: Error in JSON" );
        }
    }
    public boolean login(final String phone, String pass, final Context mContext, final SessionManager sessionManager){
        try{
            FirebaseApp.initializeApp(mContext);
            String url = Connection.getLogin();
            JSONObject obj = new JSONObject();

            obj.put("phone",phone);
            obj.put("pass",pass);
            obj.put("firebase", FirebaseInstanceId.getInstance().getToken());
            JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        if (response.has("token")) {
                            sessionManager.setToken(response.getString("token"));
                            sessionManager.setUserName(response.getJSONObject("data").getString("firstName") + response.getJSONObject("data").getString("lastName"));
                            sessionManager.setUser(phone);
                            sessionManager.setRes(true);
                        }
                        else
                            sessionManager.setRes(false);
                    }catch(JSONException e){
                        Log.e(TAG, "onResponse: JSON Exception" );
                        sessionManager.setRes(false);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(mContext, "Network Issue", Toast.LENGTH_SHORT).show();
                    sessionManager.setRes(false);

                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    return params;
                }
            };
            Volley.newRequestQueue(mContext).add(post);
            return sessionManager.getRes();
        }catch(JSONException e){
            Log.e(TAG, "login: JSON Exception" );
            return false;
        }

    }

    public void signUp(final SessionManager sessionManager, final Context mContext, String phone, String password, final SignUp signUp){
        sessionManager.setRes(false);
        JSONObject obj = new JSONObject();
        JSONArray arr = new JSONArray();
        try{

            obj.put("phone", phone);
            obj.put("pass",password);
            arr.put(obj);
        }catch(JSONException e){
            Log.e(TAG, "signUp: Error creating JSON" );

        }
        JsonArrayRequest post = new JsonArrayRequest(Request.Method.POST, Connection.getSignUp(), arr, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if(response.length() != 2){
                    sessionManager.setRes(false);
                    Toast.makeText(mContext, "Can't Send OTP", Toast.LENGTH_SHORT).show();
                }
                else {
                    try {
                        sessionManager.setRes(true);
                        sessionManager.setToken(response.getJSONObject(0).getString("token"));
                        sessionManager.setLogin(true);
                        sessionManager.setProfile(false);
                        Toast.makeText(mContext, "OTP Sent", Toast.LENGTH_SHORT).show();
                        signUp.changeActivity();
                    }catch(JSONException e){
                        Log.e(TAG, "onResponse: Error in JSON" );
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: Server Error" );
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        Volley.newRequestQueue(mContext).add(post);
        Log.e(TAG, "signUp: " + sessionManager.getRes() );

    }

    public void updateUserDetails(final ProfileDetails mCallback, final SessionManager sessionManager, Context mContext, final String firstname, final String lastname, String gender, String dob){
        JSONObject obj = new JSONObject();
        try{
            obj.put("firstName", firstname);
            obj.put("lastName",lastname);
            obj.put("gender",gender);
            obj.put("dob",dob);
            obj.put("userId",sessionManager.getUser());
        }catch(JSONException e){
            Log.e(TAG, "updateUserDetails: Error creating JSON" );
        }
        JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Connection.getUpdateDetails(), obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "onResponse: Response " + response.toString() );
                try{
                    if(response.getString("data").matches("Save Success")){
                        sessionManager.setUserName(firstname + " " + lastname);
                        sessionManager.setProfile(true);
                        mCallback.onSuccess();
                    }
                }catch(JSONException e){
                    Log.e(TAG, "onResponse: Error in JSON" );
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: Error in JSON" );
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        Volley.newRequestQueue(mContext).add(post);

    }

    public void logOut(final SessionManager sessionManager, final MainActivity mCallback, Context mContext){
        JSONObject obj = new JSONObject();
        try{
            obj.put("userId",sessionManager.getUser());
        } catch(JSONException e){
            Log.e(TAG, "logOut: Error in JSON" );
        }
        JsonObjectRequest post = new JsonObjectRequest(Request.Method.POST, Connection.getLogOut(), obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "onResponse: Response " + response.toString());
                try {
                    if (response.getString("data").matches("Save Success")) {
                        sessionManager.setUserName("");
                        sessionManager.setProfile(true);
                        sessionManager.setLogin(false);
                        sessionManager.setToken("");
                        sessionManager.setUser("");
                        mCallback.onSuccess();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: Error in JSON");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: Error in response" );
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        Volley.newRequestQueue(mContext).add(post);
    }
}
