package in.donationclub.smartstorage;

import android.util.Pair;

import java.io.Serializable;

public class GroceryItemsInfo implements Serializable {
    Pair<String, Double> items;

    public GroceryItemsInfo(Pair<String, Double> items) {
        this.items = items;
    }
}
