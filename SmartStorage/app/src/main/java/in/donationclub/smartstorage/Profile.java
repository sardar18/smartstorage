package in.donationclub.smartstorage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Profile extends AppCompatActivity {
    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    private ArrayList<BaseMessage> messageList;
    private static final String TAG = "Profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_popup);
        messageList = new ArrayList<BaseMessage>();
        messageList.add(new BaseMessage("Hey.!\nMy name is BotBot and you can ask me any questions about the app or its usage."
                , "2", "6:00"));
        messageList.add(new BaseMessage("Tell me the name of this app.?","1","6:01"));
        messageList.add(new BaseMessage("Its not decided yet these fuckers are still arguing over \"Butt Hair\"", "2", "6:01"));
        Log.e(TAG, "onCreate: size is " + messageList.size() );
        mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
        mMessageAdapter = new MessageListAdapter(this, messageList);
        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        mMessageRecycler.setAdapter(mMessageAdapter);
    }
}
