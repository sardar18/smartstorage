package in.donationclub.smartstorage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class LoginAcctivity extends AppCompatActivity {
    private TextView transit;
    private TextView phone;
    private TextView pass;
    private Button btnLogin;
    private SessionManager sessionManager;
    private RestController restController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_acctivity);
        init();
        transit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginAcctivity.this, SignUp.class);
                startActivity(i);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });

    }

    private void checkLogin() {
        if(phone.getText().toString().matches(""))
            Toast.makeText(this, "Enter Phone number",Toast.LENGTH_SHORT).show();
        else if(pass.getText().toString().matches(""))
            Toast.makeText(this, "Enter Password",Toast.LENGTH_SHORT).show();
        else{
            if(restController.login(phone.getText().toString(), pass.getText().toString(), this, sessionManager))
            {
                Intent i = new Intent(LoginAcctivity.this, MainActivity.class);
                Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show();
                sessionManager.setLogin(true);
                startActivity(i);
                finish();

            }
            else
                Toast.makeText(this, "Login Unsuccessful", Toast.LENGTH_SHORT).show();
        }
    }

    public void init(){
        transit = findViewById(R.id.tvSignIn);
        phone = findViewById(R.id.phone_login);
        pass = findViewById(R.id.pass_login);
        btnLogin = findViewById(R.id.btn_signin);
        sessionManager = new SessionManager(this);
        restController = new RestController();
    }

}
