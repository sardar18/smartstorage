package in.donationclub.smartstorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class GroceriesAdapter extends RecyclerView.Adapter<GroceriesAdapter.RecyclerViewHolder>{

    private Context mContext;
    private  ArrayList<Pair <String, Double>> items;
    private static final String TAG = "GroceriesAdapter";
    private Maps maps;

    public GroceriesAdapter(Context mContext, ArrayList<Pair<String, Double>> items) {
        this.mContext = mContext;
        this.items = items;
        Log.e(TAG, "NotificationAdapter: size" + items.size() );
        this.maps = new Maps();
    }

    @NonNull
    @Override
    public GroceriesAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.groceries_layout, viewGroup, false);
        return new GroceriesAdapter.RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GroceriesAdapter.RecyclerViewHolder recyclerViewHolder, int i) {
        Pair<String, Double> info = items.get(i);
        recyclerViewHolder.itemName.setText(info.first);
        recyclerViewHolder.quantity.setText(Double.toString(info.second));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }




    static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;
        EditText quantity;


        public RecyclerViewHolder(View v){
            super(v);
            itemName = v.findViewById(R.id.grocery_item);
            quantity = v.findViewById(R.id.grocery_quantity);

        }
    }
}
