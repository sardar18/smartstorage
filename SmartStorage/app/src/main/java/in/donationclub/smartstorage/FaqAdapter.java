package in.donationclub.smartstorage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.RecyclerViewHolder>{

    private Context mContext;
    private ArrayList<FaqInfo> items;
    private static final String TAG = "FaqAdapter";
    private Maps maps;

    public FaqAdapter(Context mContext, ArrayList<FaqInfo> items) {
        this.mContext = mContext;
        this.items = items;
        Log.e(TAG, "NotificationAdapter: size" + items.size() );
        this.maps = new Maps();
    }

    @NonNull
    @Override
    public FaqAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.faq_question_layout, viewGroup, false);
        return new FaqAdapter.RecyclerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FaqAdapter.RecyclerViewHolder recyclerViewHolder, int i) {
        FaqInfo info = items.get(i);
        recyclerViewHolder.itemName.setText(info.getQuestion());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public FaqInfo getItem(int i){
        return items.get(i);
    }


    static class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;

        public RecyclerViewHolder(View v){
            super(v);
            itemName = v.findViewById(R.id.question);

        }
    }
}
