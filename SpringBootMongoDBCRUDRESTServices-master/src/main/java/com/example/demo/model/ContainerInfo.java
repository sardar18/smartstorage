package com.example.demo.model;

import org.springframework.data.annotation.Id;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Document(collection = "Container")
public class ContainerInfo {
	@Id
	@JsonProperty(required = true, value = "containerId")
	private String containerId;
	
	@JsonProperty(required = true, value = "userId")
	private String userId;	
	
	@JsonProperty(required = true, value = "itemName")
	private String itemName;
	
	@JsonProperty(required = true, value = "threshold")
	private double threshold;
	
	@JsonProperty(required = true, value = "maxWeight")
	private double maxWeight;
	
	@JsonProperty(required = true, value = "timeLapse")
	private int timeLapse;
	
	@JsonProperty(required = true, value = "category")
	private int categpry;

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
	
	

}
