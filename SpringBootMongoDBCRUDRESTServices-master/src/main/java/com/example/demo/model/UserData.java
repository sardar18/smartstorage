package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "UserData")
public class UserData {
	
	@Id
	String id;
	String user;
	String hash;
	boolean isLoggedIn;
	String firebaseToken;
	String firstName;
	String lastName;
	String gender;
	String dob;
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public boolean isLoggedIn() {
		return isLoggedIn;
	}
	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}
	public String getFirebaseToken() {
		return firebaseToken;
	}
	public void setFirebaseToken(String firebaseToken) {
		this.firebaseToken = firebaseToken;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public UserData(String user, String hash, boolean isLoggedIn, String firebaseToken, String firstName,
			String lastName, String gender, String dob) {
		super();
		this.user = user;
		this.hash = hash;
		this.isLoggedIn = isLoggedIn;
		this.firebaseToken = firebaseToken;
		this.firstName = firstName;
		this.lastName = lastName;
//		this.phone = phone;
		this.gender = gender;
		this.dob = dob;
	}
	@Override
	public String toString() {
		return "UserData [id=" + id + ", user=" + user + ", hash=" + hash + ", isLoggedIn=" + isLoggedIn
				+ ", firebaseToken=" + firebaseToken + ", firstName=" + firstName + ", lastName=" + lastName
				+ ",  gender=" + gender + ", dob=" + dob + "]";
	}
	
	
	

}
