package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "FaqData")
public class FaqData {

	@Id
	String questionId;
	String question;
	String answer;
	
	public FaqData( String question, String answer) {
		super();
//		this.questionId = questionId;
		this.question = question;
		this.answer = answer;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "FaqData [questionId=" + questionId + ", question=" + question + ", answer=" + answer + "]";
	}
	
	
}
