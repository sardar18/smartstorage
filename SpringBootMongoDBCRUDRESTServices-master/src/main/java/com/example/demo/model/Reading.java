package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;


@Document(collection = "Reading")
public class Reading {
	@Id
	String id;
	
//	@JsonProperty(required = true, value = "userId")
//	String userId;
	
	@JsonProperty(required = true, value = "containerId")
	String containerId;
	
	@JsonProperty(required = true, value = "weight")
	double weight;
	
	@JsonProperty(required = true, value = "date")
	String date;
	
	@JsonProperty(required = true, value = "time")
	String time;
	
	@JsonProperty(required = true, value = "isFallen")
	boolean isFallen;

	public Reading(String containerId, double weight, String date, String time, boolean isFallen) {
		super();
		this.containerId = containerId;
		this.weight = weight;
		this.date = date;
		this.time = time;
		this.isFallen = isFallen;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public boolean isFallen() {
		return isFallen;
	}

	public void setFallen(boolean isFallen) {
		this.isFallen = isFallen;
	}

	
	
	
	
	
}
