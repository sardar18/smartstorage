package com.example.demo.model;

public class OTPInfo {
	String auth_key = "269421ATd4ISRs5c9afa20";
	String msg = "Your OTP is ##OTP##. Thanks for using Smart Storage";
	String sender = "SMT121";
	String phone;
	public OTPInfo(String phone) {
		super();
		this.phone = phone;
	}
	public String getAuth_key() {
		return auth_key;
	}
	public void setAuth_key(String auth_key) {
		this.auth_key = auth_key;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	

}
