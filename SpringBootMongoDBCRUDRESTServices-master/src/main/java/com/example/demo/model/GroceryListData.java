package com.example.demo.model;

import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Document(collection = "GroceryLists")
public class GroceryListData {

	@Id
	String id;
//	@JsonProperty(required = true, value = "userId")
	String userId;
//	@JsonProperty(required = true, value = "groceryName")
	String groceryName;
//	@JsonProperty(required = true, value = "groceryItems")
	ArrayList<GroceryItem> groceryItems;
	
	public GroceryListData(String userId,String groceryName, ArrayList<GroceryItem> groceryItems) {
		
		this.userId = userId;
		this.groceryName = groceryName;
		this.groceryItems = groceryItems;
	}
	
}
