package com.example.demo.model;

public class GroceryItem {
	
	
	String itemName;
	double quantity;
	
	public GroceryItem(String itemName, double quantity) {
		super();
		this.itemName = itemName;
		this.quantity = quantity;
	}
	
	

}
