package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Document(collection = "ReadingHistoryData")
public class ReadingHistoryData {
	
	@Id
	String id;
	String userId;
	String containerId;
	double weight;
	String date;
	String time;
	boolean isFallen;
	public ReadingHistoryData(String userId, String containerId, double weight, String date, String time,
			boolean isFallen) {
		super();
		this.userId = userId;
		this.containerId = containerId;
		this.weight = weight;
		this.date = date;
		this.time = time;
		this.isFallen = isFallen;
	}
	

}
