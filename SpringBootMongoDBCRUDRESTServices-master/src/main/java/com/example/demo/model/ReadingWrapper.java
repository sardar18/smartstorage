package com.example.demo.model;

import java.util.Objects;

public class ReadingWrapper {
	private Reading e;

    public ReadingWrapper(Reading e) {
        this.e = e;
    }

    public Reading unwrap() {
        return this.e;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadingWrapper that = (ReadingWrapper) o;
        return Objects.equals(e.getDate(), that.e.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(e.getDate());
    }

}
