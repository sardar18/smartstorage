package com.example.demo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Document(collection = "NotificationData")
public class NotificationData {
	
	@Id
	String notificationId;
	
	@JsonProperty(required = true, value = "notificationText")
	String notificationText;
	
	@JsonProperty(required = true, value = "notificationTime")
	String notificationTime;
	
	@JsonProperty(required = true, value = "notificationData")
	String notificationDate;
	
	@JsonProperty(required = true, value = "userId")
	String userId;

	@JsonProperty(required = true, value = "severity")
	int severity;
	
	@JsonProperty(required = true, value = "containerId")
	String containerId;
	

}
