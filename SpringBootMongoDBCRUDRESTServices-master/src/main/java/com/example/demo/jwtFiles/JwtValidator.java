package com.example.demo.jwtFiles;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

import com.example.demo.model.JwtUser;

@Component
public class JwtValidator {


    private String secret = "captainMarvel";

    public JwtUser validate(String token) {

        JwtUser jwtUser = null;
        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser("", "");

            jwtUser.setUserName(body.getSubject());
           
            jwtUser.setPassWord((String) body.get("passWord"));
        }
        catch (Exception e) {
            System.out.println(e);
        }

        return jwtUser;
    }
}
