package com.example.demo.repository;



import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Reading;

@Repository
public interface ReadingRepository extends MongoRepository<Reading, String>{
   public List<Reading> findByContainerId(String firstName, Sort sort);
   
   
   
}
