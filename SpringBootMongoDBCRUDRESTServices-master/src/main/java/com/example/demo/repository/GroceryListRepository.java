package com.example.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.demo.model.GroceryListData;
import com.example.demo.model.Reading;

public interface GroceryListRepository extends MongoRepository<GroceryListData, String> {
	
	public List<GroceryListData> findByUserId(String userId);

}
