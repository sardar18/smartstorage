package com.example.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


import com.example.demo.model.NotificationData;

@Repository
public interface NotificationRepository extends MongoRepository<NotificationData, String>{
	
	public List<NotificationData> findByUserId(String id);

}
