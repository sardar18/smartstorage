package com.example.demo.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.ReadingHistoryData;

@Repository
public interface ReadingHistoryRepository extends MongoRepository<ReadingHistoryData, String>{
	
	public ReadingHistoryData findByContainerId(String id);

}
