package com.example.demo.repository;

import java.util.List;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.ContainerInfo;




@Repository
public interface ContainerRepository extends MongoRepository<ContainerInfo, String>{
	
	 public ContainerInfo findByContainerId(String firstName);
	 
	 public List<ContainerInfo> findByUserId(String id);
}
