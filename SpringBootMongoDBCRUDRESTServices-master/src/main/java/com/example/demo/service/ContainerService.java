package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ContainerInfo;

import com.example.demo.repository.ContainerRepository;
import com.google.gson.Gson;

@Service
public class ContainerService {
	
	@Autowired
	private ContainerRepository personRepository;
	
	//Create operation
//	public ContainerInfo create(String uid, String itemName, double threshold, double maxWeight) {
//		return personRepository.save(new ContainerInfo(uid, itemName, threshold, maxWeight, 2));
//	}
	//Retrieve operation
	public String getAll(){
		String json = new Gson().toJson(personRepository.findAll());
		return "{\"data\" : " + json + "}";
	}
	public ContainerInfo getByFirstName(String firstName) {
		return personRepository.findByContainerId(firstName);
	}
	//Update operation
//	public Reading update(String firstName, String lastName, double age) {
//		Reading p = personRepository.findByContId(firstName);
//		p.setContId(lastName);
//		p.setWeight(age);
//		return personRepository.save(p);
//	}
	//Delete operation
	public void deleteAll() {
		personRepository.deleteAll();
	}
	public void delete(String firstName) {
		ContainerInfo p = personRepository.findByContainerId(firstName);
		personRepository.delete(p);
	}

}
