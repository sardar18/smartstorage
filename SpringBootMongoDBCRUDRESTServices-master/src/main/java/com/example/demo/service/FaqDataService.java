package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.FaqData;
import com.example.demo.repository.FaqRepository;

@Service
public class FaqDataService {
	
	@Autowired
	private FaqRepository faqRepository;
	
	public FaqData create(String question, String answer) {
		return faqRepository.save(new FaqData( question, answer));
	}

}
