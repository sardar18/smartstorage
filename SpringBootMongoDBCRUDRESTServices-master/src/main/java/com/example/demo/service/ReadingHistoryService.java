package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.ReadingHistoryData;
import com.example.demo.repository.ReadingHistoryRepository;

@Service
public class ReadingHistoryService {
	
	@Autowired
	private ReadingHistoryRepository readingHistoryRepository;
	
	public ReadingHistoryData create(String userId, String containerId, double weight, String date, String time,
			boolean isFallen) {
		return readingHistoryRepository.save(new ReadingHistoryData(userId, containerId, weight, date, time, isFallen));
	}

}
