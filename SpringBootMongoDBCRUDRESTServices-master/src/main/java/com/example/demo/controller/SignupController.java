package com.example.demo.controller;

import org.json.JSONException;

import java.math.BigInteger;
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.jwtFiles.JwtGenerator;
import com.example.demo.model.JwtUser;
import com.example.demo.model.UserData;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/signUp")
public class SignupController {
	
	private JwtGenerator jwtGenerator;
	
	@Autowired
	UserRepository userRepository;

    public SignupController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }
    
    @PostMapping
    public String generate(@RequestBody final String obj) {
    	try {
    		JSONObject objj = new JSONObject(obj);
    		String phone = objj.getString("phone");
    		String pass = objj.getString("pass");
    		String hashtext = "";
    		 try { 
    			  MessageDigest md = MessageDigest.getInstance("SHA-256"); 
    	            byte[] messageDigest = md.digest(pass.getBytes()); 
    	            BigInteger no = new BigInteger(1, messageDigest);
    	            hashtext = no.toString(16); 
    	            while (hashtext.length() < 32) { 
    	                hashtext = "0" + hashtext; 
    	            } 
    	        } 
    	  
    	        catch (NoSuchAlgorithmException e) { 
    	        	return "{\"data\" : \"No Such Algorithm Exception\"}";
    	        } 
    		userRepository.save(new UserData(phone, hashtext, true, "eUIKJdIs-4Q:APA91bFqx0FlrovaIAKY9UnvxB-HQEzjxtFSPF8v4BDo5lnB7-t-gAyKlpnLVui8Bgjx8munQMcx753IKvqv8L-O8sWmXdEoCbMlfRtzkwREY37eVdvpdjqc6XFJPXvD8w8oSXIDYUey", "", "", "", ""));
    		return "{\"token\" : \"" + jwtGenerator.generate(new JwtUser(phone,pass)).toString() + "\"}";
    	} catch(JSONException e) {
    		return "{\"data\" : \"JSON Exception\"}";
    	}
        

    }

}
