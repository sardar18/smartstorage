package com.example.demo.controller;



import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.jwtFiles.JwtGenerator;
import com.example.demo.model.JwtUser;
import com.example.demo.model.UserData;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping("/login")
public class LoginController {


    private JwtGenerator jwtGenerator;
    
    @Autowired
    UserRepository userRepository;

    public LoginController(JwtGenerator jwtGenerator) {
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping
    public String generate(@RequestBody final String obj) {
    	try {
    		JSONObject objj = new JSONObject(obj);
    		String phone = objj.getString("phone");
    		String pass = objj.getString("pass");
    		String hashtext = "";
    		try { 
  			  MessageDigest md = MessageDigest.getInstance("SHA-256"); 
  	            byte[] messageDigest = md.digest(pass.getBytes()); 
  	            BigInteger no = new BigInteger(1, messageDigest);
  	            hashtext = no.toString(16); 
  	            while (hashtext.length() < 32) { 
  	                hashtext = "0" + hashtext; 
  	            } 
  	        } 
  	  
  	        catch (NoSuchAlgorithmException e) { 
  	        	return "{\"data\" : \"No Such Algorithm Exception\"}";
  	        } 
    	    UserData d = userRepository.findByUser(phone);
    	    if(d.getHash().matches(hashtext))
    		return "{\"token\" : \"" + jwtGenerator.generate(new JwtUser(phone,pass)).toString() + "\"}";
    	    else
    	    	return "{\"data\" : \"No User Found\"}";
    	} catch(JSONException e) {
    		return "{\"data\" : \"JSON Exception\"}";
    	}
        

    }
}
