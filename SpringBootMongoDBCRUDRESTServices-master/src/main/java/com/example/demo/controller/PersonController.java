package com.example.demo.controller;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.modeler.NotificationInfo;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import static java.util.stream.Collectors.toCollection;

import com.example.demo.model.ContainerInfo;
import com.example.demo.model.FaqData;
import com.example.demo.model.GroceryItem;
import com.example.demo.model.GroceryListData;
import com.example.demo.model.NotificationData;
import com.example.demo.model.OTPInfo;
import com.example.demo.model.Reading;
import com.example.demo.model.ReadingWrapper;
import com.example.demo.model.UserData;
import com.example.demo.repository.ContainerRepository;
import com.example.demo.repository.FaqRepository;
import com.example.demo.repository.GroceryListRepository;
import com.example.demo.repository.NotificationRepository;
import com.example.demo.repository.ReadingRepository;
import com.example.demo.repository.UserRepository;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mongodb.BasicDBObject;
import static java.util.stream.Collectors.collectingAndThen;


@RestController

public class PersonController {
	
	@Autowired
	private ContainerRepository containerRepository;
	
	@Autowired
	private ReadingRepository readingRepository;
	
	@Autowired
	private NotificationRepository notificationRepository;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private GroceryListRepository groceryListRepository;
	
	@Autowired
	private LoginController loginController;
	
	@Autowired
	private FaqRepository faqRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AndroidPushNotificationController androidController;

	
	
	@RequestMapping ("/deleteAll")
	public String deleteAll() {
//		personService.deleteAll();
		return "Deleted all records";
	}
	
//	@RequestMapping (value = "/addContainer", method = RequestMethod.POST, produces = "application/json")
	@RequestMapping (value = "/addContainer", produces = "application/json", method = RequestMethod.POST)
	public String addContainer(@RequestBody ContainerInfo info ) {
		
		containerRepository.save(info);
		return "{\"data\" : \"Save Success\" }";
	}
	
	@RequestMapping (value = "/getContainers", produces = "application/json", method = RequestMethod.GET)
	public String getContainer() {
		String json = new Gson().toJson(containerRepository.findAll());
		return "{\"data\" : " + json + "}";
	}
	
	@RequestMapping (value = "/getAll", produces = "application/json", method = RequestMethod.POST)
	public String getContainerById(@RequestBody String obj) {
//		String json = new Gson().toJson(containerRepository.findByUserId(""))
		JSONObject objj = null;
		try {
			objj = new JSONObject(obj);
			Sort sort = new Sort(Sort.Direction.DESC, "time");
			List<ContainerInfo> arr = containerRepository.findByUserId(objj.getString("userId"));
			List<Reading> read = new ArrayList<>();
//			List<Reading> tmp = readingRepository.findByContainerId(arr.get(0).getContainerId());
//			read.add(tmp.get(0));
			for(int i = 0;i< arr.size();i++) {
				List<Reading> tmp1 = readingRepository.findByContainerId(arr.get(i).getContainerId(),sort);
				
				read.add(tmp1.get(0));
			}
			String json = new Gson().toJson(arr);
			String json1 = new Gson().toJson(read);
			return "{\"data\" : [" + json + "," + json1 + "]}";
		} catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
		
	}
	
	@RequestMapping (value = "/getAlll", produces = "application/json", method = RequestMethod.GET)
	public String getIt(@RequestBody String obj) {
		JSONObject objj = null;
		try {
			objj = new JSONObject(obj);
			List<ContainerInfo> arr = (containerRepository.findByUserId(objj.getString("userId")));
			return "{\"data\" : " + arr.get(0).getContainerId() + "}";
		}catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}
	@RequestMapping(value = "/readings", produces = "application/json", method = RequestMethod.POST)
	public String addReading(@RequestBody Reading reading) throws JSONException {
		readingRepository.save(reading);
		ContainerInfo container = containerRepository.findByContainerId(reading.getContainerId());
		if(reading.getWeight() < container.getThreshold()) {
			UserData user  = userRepository.findByUser(container.getUserId());
			androidController.send(user.getFirebaseToken(), "Threshold Reached for " + container.getItemName(), "Your container has reached its threshold kindly consider refilling it..!!");
		}
		return "{\"data\" : \"Save Success\"}";
	}

	@RequestMapping(value = "/newNotification", produces = "application/json", method = RequestMethod.POST)
	public String addNotification(@RequestBody NotificationData notificationData) {
		
		notificationRepository.save(notificationData);
		
		return "{\"data\" : \"Save Success\"}";
	}
	
	@RequestMapping(value = "/getNotifications", produces = "application/json", method = RequestMethod.POST)
	public String getNotifications(@RequestBody String obj) {
		JSONObject objj = null;
		try {
			objj = new JSONObject(obj);
			LookupOperation lookupOperation = LookupOperation.newLookup().
		            from("Container").
		            localField("containerId").
		            foreignField("_id").
		            as("post");
			AggregationOperation match = Aggregation.match(Criteria.where("post").size(1));
			Aggregation aggregation = Aggregation.newAggregation(lookupOperation, match);
			List<BasicDBObject> results = mongoTemplate.aggregate(aggregation, "NotificationData", BasicDBObject.class).getMappedResults();
			
			return "{\"data\" : " + new Gson().toJson(results) + "}";
		}catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}
	
	@RequestMapping(value = "/generateGroceryList", produces = "application/json", method = RequestMethod.POST)
	public String generateGroceryList(@RequestBody String obj) {
		JSONObject objj = null;
		try {
			objj = new JSONObject(obj);
			Sort sort = new Sort(Sort.Direction.DESC, "time");
		    List<ContainerInfo> arr = containerRepository.findByUserId(objj.getString("userId"));
		    JSONObject output = new JSONObject();
		    for(int i = 0;i<arr.size();i++) {
		    	ContainerInfo tmp = arr.get(i);
		    	Reading tmpReading = readingRepository.findByContainerId(tmp.getContainerId(),sort).get(0);
		    	if(tmpReading.getWeight() <= tmp.getThreshold())
		    		output.put(tmp.getItemName(), 1);
		    }
		    return "{\"data\" : " + output + "}";
		}catch(JSONException e) {
		return "{\"data\" : \"JSON Exception\"}";
		}
	}
	
	@RequestMapping(value = "/allGroceryLists", produces = "application/json", method = RequestMethod.POST)
	public String AllGrocery(@RequestBody String obj) {
		JSONObject objj = null;
		try {
			objj = new JSONObject(obj);
			List<GroceryListData> arr = groceryListRepository.findByUserId(objj.getString("userId"));
			return "{\"data\" : " + new Gson().toJson(arr) + "}";
		}catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}
	
	
	@RequestMapping(value = "/createGroceryList", produces = "application/json", method = RequestMethod.POST)
	public String createGroceryList(@RequestBody String object) {
	
		try {
			JSONObject obj = new JSONObject(object);
			String userId = obj.getString("userId");
			String groceryName = obj.getString("groceryName");
			JSONArray arr = obj.getJSONArray("groceryItems");
			ArrayList<GroceryItem> groceryItem = new ArrayList<>();
			for(int i = 0;i<arr.length();i++) {
				JSONObject tmp = arr.getJSONObject(i);
				String key = tmp.keys().next().toString();
				GroceryItem tmp1 = new GroceryItem(key, tmp.getDouble(key));
				groceryItem.add(tmp1);
				
			}
			groceryListRepository.save(new GroceryListData(userId, groceryName, groceryItem));
			return "{\"data\" : \"Save Success\"}";
		}catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}

	
	@RequestMapping(value = "/sendOtp", produces = "application/json", method = RequestMethod.POST)
	public String sendOtp(@RequestBody String phone) {
		try {
			JSONObject obj = new JSONObject(phone);
			OTPInfo info = new OTPInfo(obj.getString("phone"));
//			final String uri = "http://control.msg91.com/api/sendotp.php?authkey=269421ATd4ISRs5c9afa20&message=Yo%20Fucker%20save%20this%20otp%23%23OTP%23%23&sender=IM6969&mobile=919654840919";
//			RestTemplate restTemplate = new RestTemplate();
//			String response = restTemplate.getForObject(uri, String.class);
			HttpResponse<String> response = Unirest.post("http://control.msg91.com/api/sendotp.php?authkey=269421ATd4ISRs5c9afa20&message=Yo%20Fucker%20save%20this%20otp%23%23OTP%23%23&sender=IM6969&mobile=91" + info.getPhone())
			  .header("Content-Type", "application/json")
			  .asString();
			return response.getBody();
		}catch(Exception e) {
			HttpResponse<String> s = null;
//			return s;
			 return "{\"data\" : \"JSON Exception\"}";
		}
	}
	
	@RequestMapping(value = "/verifyOtp", produces = "application/json", method = RequestMethod.POST)
	public String verifyOtp(@RequestBody String obj) {
		try {
			JSONObject objj = new JSONObject(obj);
			String otp = objj.getString("otp");
			String phone = objj.getString("phone");
			HttpResponse<String> response = Unirest.post("https://control.msg91.com/api/verifyRequestOTP.php?authkey=269421ATd4ISRs5c9afa20&mobile=91" + phone + "&otp=" + otp)
					.header("Content-Type", "appliction/json")
					.asString();
			JSONObject res = new JSONObject(response.getBody());
			if(res.has("type") && res.getString("type").matches("success"))
				return loginController.generate(obj);
			else return response.getBody();
			
		}catch(Exception e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}
	
	@RequestMapping(value = "/createFaq", produces = "application/json", method = RequestMethod.POST)
	public String createFaq(@RequestBody FaqData faq) {
		faqRepository.save(faq);
		return "{\"data\" : \"Save Success\"}";
	}
	
	@RequestMapping(value = "/getFaq", produces = "application/json", method = RequestMethod.GET)
	public String getFaq() {
		return "{\"data\" : " + new Gson().toJson(faqRepository.findAll()) + "}";
	}
	
	@RequestMapping(value = "/updateUserDetails", produces = "application/json", method = RequestMethod.POST)
	public String update(@RequestBody String obj) {
		try {
			JSONObject objj = new JSONObject(obj);
			String firstName = objj.getString("firstName");
			String lastName = objj.getString("lastName");
			String gender = objj.getString("gender");
			String dob = objj.getString("dob");
			String userId = objj.getString("userId");
			UserData d = userRepository.findByUser(userId);
			d.setDob(dob);
			d.setFirstName(firstName);
			d.setGender(gender);
			d.setLastName(lastName);
			userRepository.save(d);
			return "{\"data\" : \"Save Success\"}";
		}catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}
	
	@RequestMapping(value = "/getUsage", produces = "application/json", method = RequestMethod.POST)
	public String getUsage(@RequestBody String obj) {
		try {
			JSONObject objj = new JSONObject(obj);
			Sort sort = new Sort(Sort.Direction.ASC, "time","date");
			
			List<Reading> arr = readingRepository.findByContainerId(objj.getString("containerId"), sort);
			List<Reading> unique = arr.stream().map(ReadingWrapper::new).distinct().map(ReadingWrapper::unwrap).collect(Collectors.toList());
			return "{\"data\" :" + new Gson().toJson(unique) + "}";
		}catch(JSONException e) {
			return "{\"data\" : \"JSON Exception\"}";
		}
	}
}
