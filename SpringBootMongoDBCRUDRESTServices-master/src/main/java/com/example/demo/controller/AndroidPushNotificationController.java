package com.example.demo.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpEntity;

import com.example.demo.service.AndroidPushNotificationsService;

@RestController
public class AndroidPushNotificationController {
	
	 @Autowired
	  AndroidPushNotificationsService androidPushNotificationsService;
	 
	 @RequestMapping(value = "/send", method = RequestMethod.GET, produces = "application/json")
	 public ResponseEntity<String> send(String token, String title, String text) throws JSONException {
		 
//		 JSONObject objj = new JSONObject(obj);
		 
		    JSONObject body = new JSONObject();
		    body.put("to", token);
		    body.put("priority", "high");
		 
		    JSONObject notification = new JSONObject();
		    notification.put("title", title);
		    notification.put("body", text);
		    
		    JSONObject data = new JSONObject();
		    data.put("Key-1", "JSA Data 1");
		    data.put("Key-2", "JSA Data 2");
		 
		    body.put("notification", notification);
		    body.put("data", data);
		    
		    HttpEntity<String> request = new HttpEntity<>(body.toString());
		    
		    CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
		    CompletableFuture.allOf(pushNotification).join();
		 
		    try {
		      String firebaseResponse = pushNotification.get();
		      
		      return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
		    } catch (InterruptedException e) {
		      e.printStackTrace();
		    } catch (ExecutionException e) {
		      e.printStackTrace();
		    }
		 
		    return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
		  }

}
